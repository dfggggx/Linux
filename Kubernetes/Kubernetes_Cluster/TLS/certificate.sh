#!/bin/bash
## __author__ is humingzhe
## Email : admin@humingzhe.com
## 证书模板配置文件
##-----------CA证书配置 -----------##
cat > ca-config.json <<EOF
{
  "signing": {
    "default": {
      "expiry": "87600h"
    },
    "profiles": {
      "kubernetes": {
         "expiry": "87600h",
         "usages": [
            "signing",
            "key encipherment",
            "server auth",
            "client auth"
        ]
      }
    }
  }
}
EOF
##----------ca-csr请求颁发证书具体信息----------##
cat > ca-csr.json <<EOF
{
    "CN": "kubernetes",
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "L": "Beijing",
            "ST": "Beijing",
      	    "O": "k8s",
            "OU": "System"
        }
    ]
}
EOF
##--------上面是关于生成CA证书的,生成ca和ca-key---------##
##-----------通过cfssl命令生成上述配置的证书----------##
cfssl gencert -initca ca-csr.json | cfssljson -bare ca -
##-----执行后会生成cs的证书，使用ls -ca*可查看-----##
## ca-config.json  ca.csr  ca-csr.json  ca-key.pem  ca.pem
##-----------------------分割线-----------------------------
##---------生成关于server的证书，server证书用于api通信的加密证书--------##
cat > server-csr.json <<EOF
{
    "CN": "kubernetes",
    "hosts": [
      "127.0.0.1",
      "192.168.222.10",
      "192.168.222.11",
      "192.168.222.12",
      "kubernetes",
      "kubernetes.default",
      "kubernetes.default.svc",
      "kubernetes.default.svc.cluster",
      "kubernetes.default.svc.cluster.local"
    ],
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "L": "BeiJing",
            "ST": "BeiJing",
            "O": "k8s",
            "OU": "System"
        }
    ]
}
EOF
##-----------生成server证书--------------##
cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes server-csr.json | cfssljson -bare server
##-----ls server*查看
#server.csr  server-csr.json  server-key.pem  server.pem
##----------------------分割线---------------
##-----------------生成admin证书，admin证书用于集群管理员通过证书访问集群-------------------##
cat > admin-csr.json <<EOF
{
  "CN": "admin",
  "hosts": [],
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "CN",
      "L": "BeiJing",
      "ST": "BeiJing",
      "O": "system:masters",
      "OU": "System"
    }
  ]
}
EOF
##-------------执行下方命令-------------------##
cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes admin-csr.json | cfssljson -bare admin
## ls admin*查看生成的证书
## [root@master ssl]# ls admin*
## admin.csr  admin-csr.json  admin-key.pem  admin.pem
##-------------生成kube-proxy证书-------------------##
cat > kube-proxy-csr.json <<EOF
{
  "CN": "system:kube-proxy",
  "hosts": [],
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "CN",
      "L": "BeiJing",
      "ST": "BeiJing",
      "O": "k8s",
      "OU": "System"
    }
  ]
}
EOF
##--------执行下方命令生成kube-proxy证书----------------##
cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes kube-proxy-csr.json | cfssljson -bare kube-proxy
## [root@master ssl]# ls kube-proxy*
## kube-proxy.csr  kube-proxy-csr.json  kube-proxy-key.pem  kube-proxy.pem